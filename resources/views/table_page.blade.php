@extends('layouts.app')

@section('content')
    <div class="MyTable">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{$Title}}</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{$Description}}

                        <a href="/coupons/create"><button type="submit" class="btn btn-default DirectionLtr" style="float:left; width: 120px; text-align: center !important; margin-top: -3px; padding: 2px !important;" >צור עסק חדש</button></a>

                    </div>

                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead >
                            <tr class="DirectionRtl">
                                @foreach($fileds as $filed)
                                    <th>{{$filed}}</th>
                                @endforeach
                                @if($isEdit)
                                    <th >ערוך</th>
                                @endif
                                @if($Delete)
                                    <th>מחק</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($Categories as $item)
                                <tr class="odd gradeX">
                                    @foreach($rows as $row)
                                        <td>{{$item->$row}}</td>
                                    @endforeach
                                    @if($isEdit)
                                        <td style="text-align: center"><a href="/coupons/{{$item->id}}/edit" class="btn btn-default" role="button"><i class="fa fa-pencil-square-o"></i></a> </td>
                                    @endif
                                    @if($Delete)
                                        <form action="{{ action('CouponController@destroy', ['id' => $item->id] ) }}" method="post">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <th  style="text-align: center"><button class="btn btn-default" type="submit" > <i class="fa fa-trash-o"></i></button></th>
                                            {{csrf_field()}}
                                        </form>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

    </div>
@endsection





