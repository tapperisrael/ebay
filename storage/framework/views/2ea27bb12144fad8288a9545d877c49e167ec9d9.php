<?php $__env->startSection('content'); ?>
    <div class="MyTable">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?php echo e($Title); ?></h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php echo e($Description); ?>


                        <a href="<?php echo e($url); ?>/create"><button type="submit" class="btn btn btn-primary DirectionLtr" style="float:left; width: 120px; text-align: center !important; margin-top: -3px; padding: 2px !important;" >צור קופון חדש</button></a>
                        <a href="<?php echo e($url); ?>/create"><button type="submit" class="btn btn-default DirectionLtr" style="float:left; margin-left: 10px;  width: 120px; text-align: center !important; margin-top: -3px; padding: 2px !important;" >צור עסק חדש</button></a>
                        <a href="<?php echo e($url); ?>/create"><button type="submit" class="btn btn-default DirectionLtr" style="float:left; margin-left: 10px;  width: 120px; text-align: center !important; margin-top: -3px; padding: 2px !important;" >צור עסק חדש</button></a>
                        <a href="<?php echo e($url); ?>/create"><button type="submit" class="btn btn-default DirectionLtr" style="float:left; margin-left: 10px;  width: 120px; text-align: center !important; margin-top: -3px; padding: 2px !important;" >צור עסק חדש</button></a>
                    </div>

                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead >
                            <tr class="DirectionRtl">
                                <?php $__currentLoopData = $fileds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filed): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <th>&nbsp; <?php echo e($filed); ?></th>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <th class="W80">&nbsp; תמונה </th>
                                <th class="W80">&nbsp; חברה </th>
                                <?php if($isEdit): ?>
                                    <th class="W40">&nbsp; ערוך</th>
                                <?php endif; ?>
                                <?php if($Delete): ?>
                                    <th class="W40">&nbsp;מחק</th>
                                <?php endif; ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $Categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr class="odd gradeX">
                                    <?php $__currentLoopData = $rows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <td><?php echo e($item->$row); ?></td>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <th  style="text-align: center"><img src="" style="width: 100%;" <?php echo e($CompanyName); ?> </th>
                                    <th  style="text-align: center"><?php echo e($CompanyName); ?> </th>

                                    <?php if($isEdit): ?>
                                        <td class="W40" style="text-align: center"><a href="<?php echo e($url); ?>/<?php echo e($item->id); ?>/edit" class="btn btn-default" role="button"><i class="fa fa-pencil-square-o"></i></a> </td>
                                    <?php endif; ?>
                                    <?php if($Delete): ?>
                                            <form action="<?php echo e(action('CouponController@destroy', ['id' => $item->id] )); ?>" method="post">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <th  style="text-align: center"><button class="btn btn-default" type="submit" > <i class="fa fa-trash-o"></i></button></th>
                                                <?php echo e(csrf_field()); ?>

                                            </form>
                                    <?php endif; ?>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

    </div>
<?php $__env->stopSection(); ?>






<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>