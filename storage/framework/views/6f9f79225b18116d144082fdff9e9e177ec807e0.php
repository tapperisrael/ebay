<div class="MyTable">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo e($Title); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php echo e($Description); ?>


                     <a href="/coupons/create"><button type="submit" class="btn btn-default DirectionLtr" style="float:left; width: 120px; text-align: center !important; margin-top: -3px; padding: 2px !important;" >צור עסק חדש</button></a>

            </div>

            <!-- /.panel-heading -->
            <div class="panel-body">
                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead >
                        <tr class="DirectionRtl">
                            <?php $__currentLoopData = $fileds; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $filed): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <th><?php echo e($filed); ?></th>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php if($isEdit): ?>
                                    <th >ערוך</th>
                                <?php endif; ?>
                                <?php if($Delete): ?>
                                    <th>מחק</th>
                                <?php endif; ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $Companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="odd gradeX">
                                <?php $__currentLoopData = $rows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <td><?php echo e($item->$row); ?></td>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php if($isEdit): ?>
                                    <td style="text-align: center"><a href="/coupons/<?php echo e($item->id); ?>/edit" class="btn btn-default" role="button"><i class="fa fa-pencil-square-o"></i></a> </td>
                                    <?php endif; ?>
                                    <?php if($Delete): ?>
                                        <form action="<?php echo e(action('CouponController@destroy', ['id' => $item->id] )); ?>" method="post">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <th  style="text-align: center"><button class="btn btn-default" type="submit" > <i class="fa fa-trash-o"></i></button></th>
                                            <?php echo e(csrf_field()); ?>

                                        </form>
                                    <?php endif; ?>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

</div>