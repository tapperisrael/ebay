<?php $__env->startSection('content'); ?>
    <div class="formWrapper">
        <div class="row DirectionRtl">
            <div class="col-lg-12">
                <h1 class="page-header">הוספת קופון חדש</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row DirectionRtl" >
            <div class="col-lg-12">
                <div class="panel panel-default" >
                    <div class="panel-heading " >
                        עדכן קופון חדש
                    </div>
                    <div class="panel-body ">
                        <div class="row ">
                            <div class="col-lg-12">
                                <form role="form" action="<?php echo action('UserController@update', ['id' => $item->id ] ); ?>" method="post" >
                                    <input name="_method" type="hidden" value="PATCH" >
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>מייל הלקוח</label>
                                                <input class="form-control" value="<?php echo e($item->email); ?>" name="email">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>שם הלקוח</label>
                                                <input class="form-control" value="<?php echo e($item->name); ?>" name="name">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>טלפון הלקוח</label>
                                                <input class="form-control" value="<?php echo e($item->phone); ?>" name="phone">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>עיר</label>
                                                <input class="form-control" value="<?php echo e($item->city); ?>" name="city">
                                            </div>
                                        </div>

                                    </div>



                                    <button type="submit" class="btn btn-default DirectionLtr" style="float:left; width: 120px; text-align: center !important;" >שלח פרטים</button>
                                    <?php echo e(csrf_field()); ?>

                                </form>
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php $__env->stopSection(); ?>






<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>