<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Coupon extends Model
{
    use Mediable;
    protected $hidden = ['company_id','created_at','updated_at'];
    protected $fillable = ['title','price','description','quantity'];
    protected $with = ['mediables'];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function mediables()
    {
        return $this->hasOne(Mediables::class , 'mediable_id');
        //return $this->belongsTo(Mediables::class);
    }
}
